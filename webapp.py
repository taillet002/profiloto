""" Web-app of Automation profilometer measurements
to-do:
    1. Build / test Start & Stop behavior (last function)
"""

import os
import numpy as np
import pandas as pd
import csv
import string
import base64
import time
import cv2
import json
import io
from PIL import Image
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
import dash_core_components as dcc
import dash_html_components as html
import dash_daq as daq
# Bootstrap components for Plotly Dash - https://dash-bootstrap-components.opensource.faculty.ai/
import dash_bootstrap_components as dbc
from lib.xarm.wrapper import XArmAPI
from lib.Racks_positions import Rack
import lib.utils as U

sim_ON = True
lim = 20                # limits of ticks (1tick per sec) before faking robot disconnexion

print(f'dash (plotly) version: {dash.__version__}')

# Load all (4) racks configs into 'racks_cfg' list
racks_cfg = []
for i in range(4):
    if os.path.exists(f'.\\cfg_files\\rack_{i}.json'):
        with open(f'.\\cfg_files\\rack_{i}.json', 'r') as fp:
            racks_cfg.append(json.load(fp))
    else:
        racks_cfg.append({})

# load general waypoints json
with open(r'.\cfg_files\waypoints.json', 'r') as fp:
    wp = json.load(fp)

# Load racks config (json file)
racks_pos = []
for i in range(1,5):
    filename = f'.\cfg_files\\rack{i}.json'
    if os.path.exists(filename):
        with open(filename, 'r') as fp:
            racks_pos.append(json.load(fp))
    else:
        racks_pos.append({})

## WEB PAGE LAYOUT ---------------------------------
# app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
app = dash.Dash(__name__, meta_tags=[
                {"name": "viewport", "content": "width=device-width"}])
server = app.server

arm = XArmAPI('192.168.1.197')
spd = 120            # mm/s  - 20 is quite slow
spd_servo = 30      # °/s   - 15 is quite slow
RobotConnected = False
samples_status = {'type': pd.Series(['Empty']).repeat(17*4).to_numpy(),
                  'x': np.concatenate((np.linspace(0, 2, 17, dtype=float), np.linspace(1, 3, 17, dtype=float), np.linspace(6, 4, 17, dtype=float), np.linspace(7, 5, 17, dtype=float))),                       # only for plot - np.tile(np.linspace(0, 16, 17, dtype=int), 4)
                  'y': np.tile(np.linspace(16, 0, 17, dtype=int), 4),     # only for plot - np.flip(np.repeat(np.linspace(0, 3, 4, dtype=int), 17)
                  'age': pd.Series(['']).repeat(17*4).to_numpy(),
                  'campaign': pd.Series(['']).repeat(17*4).to_numpy(),
                  }                         # samples associated with plate type for graph
samples_selec = []                          # plates selected in graph
bad_IDs = []                                # bad IDs reading
plate_type = []                             # plate type selected by user
plate_age = []                              # aging input by user
plate_campaign = []                         # campaign name input by user
plate_graph_height = 200                    # graph heigh maximum
is_runing = False                           # whether set-up is running an automation
# [plate progression(X/68) [md1], one plate prog (%)[one_pp], all plate progress (%)[all_pp], remaining time[md2], actual plate, Led color]
status_adv = ['0/0', '0%', '0%', 'Not running', 0, '#000000']

# reading campaigns_names.txt to keep old campaigns names for autocomplete input
campaign_names = []
with open('campaigns_names.txt', 'r') as fd:
    reader = csv.reader(fd)
    for line in reader:
        campaign_names.append(line[0])

# reading projects_plates_config.txt with every
plates_cfg = pd.read_csv('projects_plates_config.txt')

app.layout = html.Div(
    [
        dcc.Store(id="aggregate_data"),
        # empty Div to trigger javascript file for graph resizing
        html.Div(id="output-clientside"),
        html.Div(   # top banner
            [
                html.Div(   # solvay logo
                    [
                        html.Img(
                            # src=app.get_asset_url("Solvay_logo_transp.png"),
                            src=app.get_asset_url("logo_Solvay_2.jpg"),
                            id="solvay-logo",
                            style={
                                "height": "120px",
                                "width": "auto",
                                # "margin-bottom": "10px",
                                # 'margin-top': f"{10:.0f}px",
                                'margin-left': f"{80:.0f}px",
                            },
                        )
                    ],
                    className="one-third column",
                ),
                html.Div(   # title of the app
                    [
                        html.Div(
                            [
                                html.H3(
                                    "Profilometer Automation",
                                    style={"color": "#d4d4d4",
                                           "margin-top": "0px",
                                           "margin-bottom": "0px", },
                                ),
                                html.H6(
                                    "Made by RIC Bordeaux", style={"margin-bottom": "0px", "color": "#787878"}
                                ),
                            ]
                        )
                    ],
                    className="one-half column",
                    id="title",
                ),
                html.Div(   # buttons
                    [
                        html.A(
                            html.Button("What it does 🎬",
                                        id="what-it-does-button",
                                        style={"color": "#d4d4d4"},
                                        ),
                            href="https://drive.google.com/file/d/1rjBXmVDxjveOXFZJRhcIgf_N9ksyk4s5/view",
                            target="_blank",
                            rel="noopener noreferrer",
                            style={"margin-top": "0px",
                                   "margin-bottom": "0px"},
                        ),
                        dbc.Tooltip(
                            f"Video of what does this set-up do",
                            target=f"what-it-does-button",
                            placement='bottom',
                            style={'background-color': '#cccccc'},
                            hide_arrow=False,
                        ),
                        html.A(
                            html.Button("How to run 🎬",
                                        id="how-to-run-button",
                                        style={"color": "#d4d4d4"},
                                        ),
                            href="https://drive.google.com/file/d/1rjBXmVDxjveOXFZJRhcIgf_N9ksyk4s5/view",
                            target="_blank",
                            rel="noopener noreferrer",
                            style={"margin-top": "0px",
                                   "margin-bottom": "0px"},
                        ),
                        dbc.Tooltip(
                            f"Video of how to make this set-up run",
                            target=f"how-to-run-button",
                            placement='bottom',
                            style={'background-color': '#cccccc'},
                            hide_arrow=False,
                        ),
                    ],
                    className="one-third column",
                    id="top_right-buttons",
                ),
            ],
            id="header",
            className="row flex-display",
            style={"margin-top": "0px", "margin-bottom": "0px"},
        ),
        html.Div(   # top row
            [
                html.Div(   # Bouton Robot connexion
                    [
                        daq.PowerButton(
                            on=RobotConnected,
                            id='power-button',
                            size=80,
                            label='Robot connexion',
                            labelPosition='top',
                        ),
                    ],
                    className="pretty_container one columns",
                    id="cross-filter-options",
                ),
                html.Div(   # graph
                    [
                        html.Div(   # graph plot
                            [
                                dcc.Graph(id="graph-plates",
                                          style={'height': plate_graph_height},
                                          config={'displayModeBar': False},
                                          figure={
                                              'data': [{
                                                  'x': samples_status['x'],
                                                  'y': samples_status['y'],
                                                  'name': 'Empty',
                                                  'mode': 'markers',
                                                  'text': [f'{i+1}.Empty' for i in range(0, 17*4)],
                                                  'hoverinfo': 'text',
                                                  'marker': {'size': 15, 'color': 'rgb(150, 150, 150)', },
                                              }],
                                              'layout': {
                                                  'xaxis': {'showgrid': False, 'zeroline': False, },
                                                  'yaxis': {'showgrid': False, 'zeroline': False, },
                                                  'dragmode': 'select',
                                                  'clickmode': 'event+select',
                                                  'margin': {'l': 0, 'r': 0, 't': 0, 'b': 0},
                                                  'showlegend': True,
                                              },
                                          }
                                          )],
                            id="countGraphContainer",
                            className="pretty_container",
                        ),
                    ],
                    className="seven columns",
                    id="right-column",
                ),
                html.Div(   # Plate positions names
                    [
                        dcc.Dropdown(   # plate list drop menue
                            id="plate_list-dropmenu",
                            options=[{'label': i, 'value': i}
                                     for i in ['Empty', 'A', 'B', 'C', 'D']],
                            multi=False,
                            # value=['Empty'],
                            style={'background-color': '#FFFFFF',
                                   'color': '#000000',
                                   'margin-top': f"{0:.0f}px",
                                   'margin-bottom': f"{5:.0f}px",
                                   'border': '0px solid #2E4053',
                                   'text-decoration': 'none',
                                   'height': f"{(plate_graph_height-((4*2)*5))/4:.0f}px",
                                   },
                        ),
                        dcc.Input(      # ageing time for plates
                            id="ageing-time",
                            type='number',
                            placeholder='corrosion chamber ageing time',
                            value='',
                            style={'background-color': '#FFFFFF',
                                   'color': '#000000',
                                   'margin-top': f"{5:.0f}px",
                                   'margin-bottom': f"{5:.0f}px",
                                   'border': '1px solid #2E4053',
                                   'text-decoration': 'none',
                                   'height': f"{(plate_graph_height-((4*2)*5))/4:.0f}px",
                                   'width': '100%',
                                   },
                        ),
                        html.Datalist(  # autocomplete list for campaigns names
                            id='list-suggested-inputs', 
                            children=[html.Option(value=word) for word in campaign_names],
                        ),
                        dcc.Input(      # campaigns names
                            id="campaign-name",
                            type='text',
                            placeholder='campaign name',
                            list='list-suggested-inputs',
                            value='',
                            debounce=True,
                            style={'background-color': '#FFFFFF',
                                   'color': '#000000',
                                   'margin-top': f"{5:.0f}px",
                                   'margin-bottom': f"{5:.0f}px",
                                   'border': '1px solid #2E4053',
                                   'text-decoration': 'none',
                                   'height': f"{(plate_graph_height-((4*2)*5))/4:.0f}px",
                                   'width': '100%',
                                   },
                        ),
                        html.Button(    # Apply to selection button
                            'Apply to selection', id='btn-aplselec',
                            style={'background-color': '#FFFFFF',
                                    'color': '#000000',
                                    'margin-top': f"{5:.0f}px",
                                    'margin-bottom': f"{5:.0f}px",
                                    'border': '1px solid #2E4053',
                                    'text-decoration': 'none',
                                    'border-radius': '8px',
                                    'width': '98%',
                                    'height': f"{(plate_graph_height-((4*2)*5))/4:.0f}px",
                                    },
                            ),
                        dbc.Tooltip(    # tooltip for Apply to selection button
                            f"Apply given type to selected plate(s)",
                            target=f"btn-aplselec",
                            placement='bottom',
                            style={'background-color': '#424242',
                                   'color': '#e8e8e8',
                                   'border': '2px solid #2E4053',
                                   'border-radius': '5px', },
                            hide_arrow=False,
                        ),
                        html.Button(    # Clear selection button
                            'Clear selection', id='btn-clrselec', disabled=False,
                            style={'background-color': '#FFFFFF',
                                    'color': '#000000',
                                    'margin-top': f"{5:.0f}px",
                                    'margin-bottom': f"{5:.0f}px",
                                    'border': '1px solid #2E4053',
                                    'text-decoration': 'none',
                                    'border-radius': '8px',
                                    'width': '98%',
                                    'height': f"{(plate_graph_height-((4*2)*5))/4:.0f}px",
                                    },
                            ),
                        dbc.Tooltip(    # tooltip for Clear selection button
                            f"Mark selected plate(s) as 'Empty'",
                            target=f"btn-clrselec",
                            placement='bottom',
                            style={'background-color': '#424242',
                                   'color': '#e8e8e8',
                                   'border': '2px solid #2E4053',
                                   'border-radius': '5px', },
                            hide_arrow=False,
                        ),
                    ],
                    className="pretty_container two columns",
                    id="Plate-positions-names",
                ),
                html.Div(   # Start Stop
                    [
                        html.Button('Start', id='btn-start', disabled=False,
                                    style={'background-color': '#307a56',
                                           'color': '#000000',
                                           'margin-top': f"{40:.0f}px",
                                           'margin-bottom': f"{15:.0f}px",
                                           'border': '1px solid #2E4053',
                                           'text-decoration': 'none',
                                           'border-radius': '8px',
                                           'width': '98%',
                                           'height': f"{(plate_graph_height-((2*2)*15))/2:.0f}px",
                                           },
                                    ),
                        html.Button('Stop', id='btn-stop',
                                    style={'background-color': '#9c2230',
                                           'color': '#000000',
                                           'margin-top': f"{15:.0f}px",
                                           'margin-bottom': f"{15:.0f}px",
                                           'border': '1px solid #2E4053',
                                           'text-decoration': 'none',
                                           'border-radius': '8px',
                                           'width': '98%',
                                           'height': f"{(plate_graph_height-((2*2)*15))/2:.0f}px",
                                           },
                                    ),
                    ],
                    className="pretty_container two columns",
                    id="Start-Stop",
                ),
            ],
            className="row flex-display",
        ),
        html.Div(   # middle row
            [
                html.Div(       # left
                    [daq.Indicator(
                        label=" ",
                        color="#bf2600",    # (rouge)          -> 1cb829 (vert)
                        value=False,
                        id='led_indicator',
                        style={"margin-bottom": "40px",},
                        ),
                    html.H1('0 / 0',
                        id='md1',
                        style={"margin-bottom": "40px",
                                },
                        ),
                    daq.GraduatedBar(
                        id='one_pp',       # one plate progress
                        label="One plate progress",
                        showCurrentValue=True,
                        max=100,
                        value=0,
                    ),
                    daq.GraduatedBar(
                        id='all_pp',       # all plate progress
                        label="All plates progress",
                        showCurrentValue=True,
                        max=100,
                        value=0,
                    ),
                    html.H6('Remaining time:',
                        id='md_Hidden1',
                        style={"margin-top": "40px",
                        },),
                    html.H1('not running',
                        id='md2',
                        style={"margin-top": "0px",
                        },),
                    ],
                    className="pretty_container two columns",
                ),
                html.Div(              # right
                    [html.Img(id="img1", src="/video_feed",
                              style={'height': '550px', "margin-right": "5px"}),
                     html.Img(id="img2", src="/video_feed",
                              style={'height': '550px', "margin-right": "15px"}),],    # 'width': '25%'
                    className="pretty_container ten columns",
                ),
            ],
            className="row flex-display",
        ),
        dcc.Interval(id='plate_list-timer', interval=1000,
                     n_intervals=0),   # inrerval in milliseconds
        dcc.Interval(id='images-timer', interval=300,
                     n_intervals=0),   # inrerval in milliseconds
        dcc.ConfirmDialog(
            id='start-error', message='Cannot start experiment. Robot not connected or all plates are Empty'),
    ],
    id="mainContainer",
    style={"display": "flex", "flex-direction": "column"},
)


## GENERAL FUNCTIONS ----------------------------------

def clean_errors(xArm):
    if xArm.has_error:
        print(f'error was: {xArm.error_code}')
    xArm.clean_error()
    xArm.set_mode(0)
    xArm.set_state(state=0)
    
    xArm.clean_gripper_error()
    xArm.set_gripper_enable(enable=True)
    xArm.set_gripper_mode(0)
    print(f'xArm errors cleaned')

def generate_fake_image(size=(600, 400)):
    ''' save a fake png image file into image folder '''
    blank_image = np.zeros((size[0], size[1], 3), np.uint8)
    # cross lines
    color_lines = (0, 0, 255)         # B G R
    x1, y1 = 10, 10
    x2, y2 = size[1] - 10, size[0] - 10
    cv2.line(blank_image, (x1, y1), (x2, y2), color_lines, thickness=6)
    cv2.line(blank_image, (x1, y2), (x2, y1), color_lines, thickness=6)
    # random bubbles
    color_bubbles = (255, 50, 50)
    for b in range(1, np.random.randint(10)):
        cv2.circle(blank_image, (np.random.randint(size[1]), np.random.randint(size[0])), np.random.randint(
            100), color_bubbles, -1)        # img, center, radius, color, thickness, lineType, shift
    cv2.imwrite('./datas/plate_picture.png', blank_image)                                               # raw image
    cv2.imwrite('./datas/plate_picture_grey.png', cv2.cvtColor(blank_image, cv2.COLOR_BGR2GRAY))             # grey level image

def fake_ID_reading():
    # one chance on 5 to be an error ID reading case
    error = np.random.choice([True, False, False, False, False])
    ID = ''.join([np.random.choice(list(string.ascii_uppercase + string.digits)) for n in range(5)])
    if error:
        ID = 'N/A'

    return error, ID

def graph_update(samples_status, status_adv=[]):
    ''' returns {'data': data, 'layout': layout} for graph update '''
    # actual selection by the user
    data = [{
        'x': samples_status['x'],
        'y': samples_status['y'],
        'mode': 'markers',
        'transforms':  [{
            'type': 'groupby',
            'groups': samples_status['type'],
            'styles': [{
                'target': 'Empty', 'value': {'marker': {'color': 'rgb(150, 150, 150)'}}
            }],
        }],
        'name': [f'{i}' for i in samples_status['type']],
        'text': [f'{i+1}.{v[0]} {v[1]} {v[2]}' for i, v in enumerate(zip(samples_status['type'], samples_status['age'], samples_status['campaign']))],
        'hoverinfo': 'text',
        # , 'color': 'rgb(150, 150, 150)', }
        'marker': {'size': 15}
    }]
    
    # cross indicator on current plate operations
    if status_adv != []:
        data.append({
            'x': [samples_status['x'][status_adv[4]]],
            'y': [samples_status['y'][status_adv[4]]],
            'name': 'Current',
            'mode': 'markers',
            'marker': {'symbol': 'star-diamond-open', 'size': 25, 'color': 'rgb(113, 199, 130)',
                    'line': {'width': 3, 'color': 'rgb(190, 0, 0)'},
                    },
        })

    # cross indicator on bad IDs
    if bad_IDs != []:
        data.append({
            'x': samples_status['x'][bad_IDs],
            'y': samples_status['y'][bad_IDs],
            'name': 'Error on ID',
            'mode': 'markers',
            'marker': {'symbol': 'x-open', 'size': 15, 'color': 'rgb(190, 0, 0)',
                    'line': {'width': 3, 'color': 'rgb(190, 0, 0)'},
                    },
        })

    layout = {
        'xaxis': {'showgrid': False, 'zeroline': False, },
        'yaxis': {'showgrid': False, 'zeroline': False, },
        'dragmode': 'select',
        'clickmode': 'event+select',
        'margin': {'l': 0, 'r': 0, 't': 0, 'b': 0},
        'showlegend': True
    }
    
    return {'data': data, 'layout': layout}


## UI CALLBACK FUNCTIONS ----------------------------------

# Timer for plate graph (+ boutons) & global advancement (bottom part)
@app.callback([dash.dependencies.Output('graph-plates', 'figure'),
               dash.dependencies.Output('img1', 'src'),
               dash.dependencies.Output('img2', 'src'),
               dash.dependencies.Output('led_indicator', 'color'),
               dash.dependencies.Output('md1', 'children'),
               dash.dependencies.Output('one_pp', 'value'),
               dash.dependencies.Output('all_pp', 'value'),
               dash.dependencies.Output('md2', 'children'), ],
              [dash.dependencies.Input('images-timer', 'n_intervals'),
               dash.dependencies.Input('btn-aplselec', 'n_clicks'),
               dash.dependencies.Input('btn-clrselec', 'n_clicks')])
def update_output(n, n_clks1, n_clcks2):
    global samples_status, status_adv, is_runing

    ctx = dash.callback_context
    if not ctx.triggered:
        print('not a trigerred event')
    else:
        button_id = ctx.triggered[0]['prop_id'].split('.')[0]

        if (button_id == 'images-timer') & (is_runing):         # if running update graph, infos and images
            # 1 - plot graph racks / plates
            upd_grph = graph_update(samples_status, status_adv)

            # 2a - plot image 1
            im = cv2.imread('./datas/plate_picture.png')
            im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
            im_pil = Image.fromarray(im)
            buff = io.BytesIO()
            im_pil.save(buff, format="png")
            im_b64 = base64.b64encode(buff.getvalue()).decode("utf-8")

            # 2b - plot image grey
            im2 = cv2.imread('./datas/plate_picture_grey.png')
            im2 = cv2.cvtColor(im2, cv2.COLOR_BGR2RGB)
            im_pil2 = Image.fromarray(im2)
            buff2 = io.BytesIO()
            im_pil2.save(buff2, format="png")
            im_b642 = base64.b64encode(buff2.getvalue()).decode("utf-8")

            return upd_grph, "data:datas/png;base64," + im_b64, "data:datas/png;base64," + im_b642, status_adv[5], status_adv[0], status_adv[1], status_adv[2], status_adv[3]

        elif (button_id == 'btn-aplselec') & (not is_runing):       # if not running and aplselec pressed
            samples_status['type'][samples_selec] = plate_type
            samples_status['age'][samples_selec] = plate_age
            samples_status['campaign'][samples_selec] = plate_campaign

            # 1 - plot graph racks / plates
            upd_grph = graph_update(samples_status)

            return upd_grph, 'no images', 'no images', '#bf2600', '0 / 0', 0, 0, 'not running'

        elif (button_id == 'btn-clrselec') & (not is_runing):       # if not running and clrselec pressed
            samples_status['type'][samples_selec] = 'Empty'
            samples_status['age'][samples_selec] = ''
            samples_status['campaign'][samples_selec] = ''

            # 1 - plot graph racks / plates
            upd_grph = graph_update(samples_status)

            return upd_grph, 'no images', 'no images', '#bf2600', '0 / 0', 0, 0, 'not running'

        raise dash.exceptions.PreventUpdate("cancel the callback")

# INTERVAL TIMER - checking for robot connexion sate (uses plate_list-timer)
prev_n_interv = 0
@app.callback([dash.dependencies.Output('power-button', 'label'),
               dash.dependencies.Output('power-button', 'on')],
              [dash.dependencies.Input('plate_list-timer', 'n_intervals')])
def update_output(n):
    global RobotConnected, prev_n_interv, arm
    # fake reading of the state
    if sim_ON: # if simulation mode = ON, fake deconnexion after 'lim' number of ticks (1sec per tick), or if running after 12 plates
        if RobotConnected:       # if state is robot connected (True)
            if is_runing:           # if run is ON
                if status_adv[4] > 4:     # after 500 plates done fake a robot connexion lost
                    RobotConnected = False
                    print(f'Robot connexion lost')
                    return (f'Robot connexion', False)    # changing state to 'OFF'
            else:                   # if run is OFF
                if abs(prev_n_interv - n) > lim:    # if 'lim' iterations (done while 'ON' state), then fake robot connexion lost
                    RobotConnected = False
                    print(f'Robot connexion lost')
                    prev_n_interv = n
                    return (f'Robot connexion', False)    # changing state to 'OFF'            
    else:   # if not in simulaton mode
        if arm:
            RobotConnected = arm.connected

    raise dash.exceptions.PreventUpdate("cancel the callback")

# Power button
@app.callback(dash.dependencies.Output('power-button', 'size'),
              [dash.dependencies.Input('power-button', 'on')])
def update_output(on):
    global RobotConnected, arm
    ctx = dash.callback_context
    # print(ctx.inputs['power-button.on'])
    if ctx.inputs['power-button.on']:          # user clicked to reconnect robot
        # reconnect to robot here
        if not sim_ON:      # if NOT in sumulation mode
            arm.connect()
            arm.motion_enable(enable=True)
            arm.set_mode(0)
            arm.set_state(state=0)
            print(f'xArm version: {arm.get_version()[1]}')
            
            arm.set_gripper_enable(enable=True)
            arm.set_gripper_mode(0)
            print(f'Gripper ready to use')
            
            # Change TCP LOAD for the whole notebook
            tcp_load = 0.82
            tcp_gravity_center = [0.0, 0.0, 48.0]
            arm.set_tcp_load(weight = tcp_load, center_of_gravity = tcp_gravity_center)
            arm.set_mode(0)
            arm.set_state(state=0)
            time.sleep(0.5)
            print(f'tcp load: {arm.tcp_load}')

            # Change TCP OFFSET for the whole notebook
            tcp_offset = [0.0, 0.0, 172.0, 0, 0, 0]
            arm.set_tcp_offset(tcp_offset)
            arm.set_mode(0)
            arm.set_state(state=0)
            time.sleep(0.5)
            print(f'tcp offset: {arm.tcp_offset}')
            
            arm.set_collision_sensitivity(2) # 0 -> destroy all, 5 -> very sensitive
            arm.set_mode(0)
            arm.set_state(state=0)
            time.sleep(0.5)
            print(f'sensivity level [1(strong) - 5(low)]: {arm.collision_sensitivity}')

            if arm:
                RobotConnected = arm.connected
        else:   # if in sumulation mode
            RobotConnected = True
        
        if RobotConnected:
            print(f'xArm robot connected')
        else:
            print(f'Error: xArm robot not connected')

    raise dash.exceptions.PreventUpdate("cancel the callback")

# Update points on graph (plates) selected
@app.callback(dash.dependencies.Output('graph-plates', 'loading_state'),
              [dash.dependencies.Input('graph-plates', 'selectedData')])
def on_click(selection):
    ''' This is trigerred whenever user select points in the graph'''
    global samples_selec
    samples_selec = []
    if selection is not None:
        # print(selection)
        for point in selection['points']:
            samples_selec.append(point['pointIndex'])
    raise dash.exceptions.PreventUpdate("cancel the callback")

# Plate list TIMER - 1000 ms
@ app.callback(dash.dependencies.Output('plate_list-dropmenu', 'options'),
               [dash.dependencies.Input('plate_list-timer', 'n_intervals')])
def update_output(n):
    ''' read projects_plates_config.txt file once per seconds, and update the options for plate_list-dropmenu '''
    df_platecfg = pd.read_csv('projects_plates_config.txt')

    return [{'label': i, 'value': i} for i in df_platecfg['name']]

# plate type new selection
@app.callback(dash.dependencies.Output('plate_list-dropmenu', 'multi'),
              [dash.dependencies.Input('plate_list-dropmenu', 'value')])
def on_click(val):
    ''' This is trigerred whenever user select a new type of plate in the plate list dropdown menu '''
    global plate_type
    plate_type = val
    raise dash.exceptions.PreventUpdate("cancel the callback")

# plate age new entry
@app.callback(dash.dependencies.Output('ageing-time', 'name'),
              [dash.dependencies.Input('ageing-time', 'value')])
def on_click(val):
    ''' This is trigerred whenever user write a new plate age '''
    global plate_age
    plate_age = val
    raise dash.exceptions.PreventUpdate("cancel the callback")

# plate campaign new entry
@app.callback(dash.dependencies.Output('list-suggested-inputs', 'children'),
              [dash.dependencies.Input('campaign-name', 'value')])
def on_click(val):
    ''' This is trigerred whenever user write a new campaign name '''
    global plate_campaign, campaign_names
    plate_campaign = val
    
    # add entry to campaigns_names.txt if it is a new one
    if ((val not in campaign_names) & (val != '')):
        campaign_names.append(val)
        of = open('campaigns_names.txt', 'a')
        of.write(f'\n{val}')
        of.close()
        return [html.Option(value=word) for word in campaign_names]

    raise dash.exceptions.PreventUpdate("cancel the callback")

# START
@app.callback(dash.dependencies.Output('start-error', 'displayed'),
              [dash.dependencies.Input('btn-start', 'n_clicks')])
def on_click(n_clks):
    global status_adv, is_runing, bad_IDs, arm
    if n_clks is not None:

        # error if only 'Empty' plates
        if (list(np.unique(samples_status['type'])) == ['Empty']) or (not RobotConnected):
            return True

        else:
            # filter non empty plates
            df_to_process = pd.DataFrame.from_dict(samples_status)
            df_to_process = df_to_process[df_to_process['type'] != 'Empty']
            
            is_runing = True
            bad_IDs = []
            
            # clean_errors(arm)

            # loop on every plate -
            for i, row in enumerate(df_to_process.iterrows()):

                # old version with row rack
                # # row on selection graph are inverted (top = 3, bottom = 0), so we reverse it
                # i_rack = 3 - row[1]["y"]        # wich rack
                # i_pos = row[1]["x"]             # wich position in the rack

                i_rack = int(np.ceil((row[0]+1)/17)-1)
                i_pos = int(row[0] - i_rack * 17)
                
                # 0 - If at home position and no errors either go to front_racks_global_wp or tail_racks_global_wp
                status_adv = [
                    f'{i+1}/{df_to_process.shape[0]}', 20, (i)*100/df_to_process.shape[0], 'x min', row[0], '#1cb829']
                if (not is_runing) or (not RobotConnected):
                    break
                print(f'\ni:{i}, rack:{i_rack}, pos:{i_pos}, {wp["front_general_srv"]}')
                if sim_ON:
                    time.sleep(1)
                else:
                    if (i_rack == 0) | (i_rack == 1):
                        print('No general rack safe waypoint yet !!!')
                    elif (i_rack == 2) | (i_rack == 3):
                        arm.set_servo_angle(servo_id=None, angle=wp["front_general_srv"], speed=spd_servo, relative=False, wait=True)
                
                # 1 - take plate from rack
                status_adv = [
                    f'{i+1}/{df_to_process.shape[0]}', 40, (i)*100/df_to_process.shape[0], 'x min', row[0], '#1cb829']
                if (not is_runing) or (not RobotConnected):
                    break
                if sim_ON:
                    time.sleep(1)
                else:
                    pos = racks_pos[i_rack]['allcoords_z_safe'][0][i_pos]
                    # print(pos)
                    arm.set_position(*pos, speed=spd, is_radian=False, wait=True)
                    # pos_2_go = U.cor_pos(R, 0, plate_pos-1, plate_angle, length+50, shift_x=0) # (rack object, row, col, plate angle, plate heigh+secure heigh) 
                    # arm.set_position(*pos_2_go, speed=speed, is_radian=False, wait=True)
                    if (i_rack == 0) | (i_rack == 1):
                        print('No general rack safe waypoint yet !!!')
                    elif (i_rack == 2) | (i_rack == 3):
                        arm.set_servo_angle(servo_id=None, angle=wp["front_general_srv"], speed=spd_servo, relative=False, wait=True)

                # 2 - bring plate to profilometer
                status_adv = [
                    f'{i+1}/{df_to_process.shape[0]}', 60, (i)*100/df_to_process.shape[0], 'x min', row[0], '#1cb829']
                if (not is_runing) or (not RobotConnected):
                    break
                if sim_ON:
                    time.sleep(1)
                else:
                    arm.set_servo_angle(servo_id=None, angle=wp['key01'], speed=spd_servo, relative=False, wait=True)
                
                # 3 - acquire profile
                if sim_ON:
                    generate_fake_image()           # this function fakes a profile saved in png in ./datas/plate_picture.png
                    plateID_error, plateID = fake_ID_reading()
                    print(f'error: {plateID_error} reading ID: {plateID}')
                    time.sleep(1)
                else:
                    plateID_error = None
                    arm.set_position(z=80, speed=spd, relative=True, wait=True)
                if plateID_error:
                    bad_IDs.append(row[0])
                print(f'bad_IDs: {bad_IDs}')
                status_adv = [
                    f'{i+1}/{df_to_process.shape[0]}', 80, (i)*100/df_to_process.shape[0], 'x min', row[0], '#1cb829']
                if (not is_runing) or (not RobotConnected):
                    break
                
                
                # 4 - bring back plate to rack
                status_adv = [
                    f'{i+1}/{df_to_process.shape[0]}', 100, (i)*100/df_to_process.shape[0], 'x min', row[0], '#1cb829']
                if sim_ON:
                    time.sleep(1)
                else:
                    if (i_rack == 0) | (i_rack == 1):
                        print('No general rack safe waypoint yet !!!')
                    elif (i_rack == 2) | (i_rack == 3):
                        arm.set_servo_angle(servo_id=None, angle=wp["front_general_srv"], speed=spd_servo, relative=False, wait=True)
                    pos = racks_pos[i_rack]['allcoords_z_safe'][0][i_pos]
                    arm.set_position(*pos, speed=spd, is_radian=False, wait=True)
                if (not is_runing) or (not RobotConnected):
                    break

                # 5 - go back to general safe waypoint
                if sim_ON:
                    time.sleep(1)
                else:
                    if (i_rack == 0) | (i_rack == 1):
                        print('No general rack safe waypoint yet !!!')
                    elif (i_rack == 2) | (i_rack == 3):
                        arm.set_servo_angle(servo_id=None, angle=wp["front_general_srv"], speed=spd_servo, relative=False, wait=True)

            if is_runing:
                status_adv = [f'{i+1}/{df_to_process.shape[0]}', 100, (i+1)*100/df_to_process.shape[0], 'not running', row[0], '#bf2600']
            else:
                status_adv = [
                    f'{i+1}/{df_to_process.shape[0]}', 100, (i)*100/df_to_process.shape[0], 'not running', row[0], '#bf2600']
            time.sleep(2)

            is_runing = False

    raise dash.exceptions.PreventUpdate("cancel the callback")

# STOP
@app.callback(dash.dependencies.Output('btn-stop', 'name'),
              [dash.dependencies.Input('btn-stop', 'n_clicks')])
def on_click(n_clks):
    global is_runing
    if n_clks is not None:
        is_runing = False

# Main
if __name__ == "__main__":
    app.run_server(debug=True)


# # test text file opening
# @app.callback(dash.dependencies.Output('btn-clrselec', 'name'),
#     [dash.dependencies.Input('btn-clrselec', 'n_clicks')])
# def on_click(n_clks):
#     if n_clks is not None:
#         # open project / plate config file with text editor
#         os.system("notepad.exe projects_plates_config.txt")
#     raise dash.exceptions.PreventUpdate("cancel the callback")
