"""
Class KeyenceProfilo
    Class obj to control LJX800A controler (profilometer)
    Python API to the LJX8_IF.dll
    Get profiles in 1 shot mode and High Speed mode.

Solvay - RICBordeaux [LoF]
2020
{tristan.aillet - jordy.bonnet - roman.koetitz}@solvay.com
"""

# import numpy as np
import ctypes as ct
from ctypes import wintypes as wt
from enum import IntEnum
import time
import re
import numpy as np
import os

############# some classical arrays ######
B_ARRAY_4 = ct.c_uint8 * 4
B_ARRAY_2 = ct.c_uint8 * 2
B_ARRAY_3 = ct.c_uint8 * 3

############# ctype structs ##############

class LJX8IF_VERSION_INFO(ct.Structure):
    """
    ###### Get DLL version

    Typedef struct {
    INT nMajorNumber;
    INT nMinorNumber;
    INT nRevisionNumber;
    INT nBuildNumber;
    } LJX8IF_VERSION_INFO;

    """
    _fields_ = [('nMajorNumber', ct.c_int),
                ('nMinorNumber', ct.c_int),
                ('nRevisionNumber', ct.c_int),
                ('nBuildNumber', ct.c_int),
                ]

class LJX8IF_ETHERNET_CONFIG(ct.Structure):
    """ 
    Typedef struct {
        BYTE abyIpAddress[4]; The IP address of the controller to connect to.
        WORD wPortNo; The port number of the controller to connect to.
        BYTE reserve[2];
    } LJX8IF_ETHERNET_CONFIG;

    """
    _fields_ = [('abyIpAddress', B_ARRAY_4),
                ('wPortNo', ct.c_uint16),
                ('reserve', B_ARRAY_2),
                ]

class LJX8IF_PROFILE_BANK(IntEnum):
    """
    Typedef enum {
    LJX8IF_PROFILE_BANK_ACTIVE = 0x00, // Active surface
    LJX8IF_PROFILE_BANK_INACTIVE = 0x01, // Inactive surface
    } LJX8IF_PROFILE_BANK;
    """
    LJX8IF_PROFILE_BANK_ACTIVE = 0x0
    LJX8IF_PROFILE_BANK_INACTIVE = 0x1

    def __init__(self,value):
        self._as_parameters = int(value)
    
class LJX8IF_PROFILE_POSITION(IntEnum):
    """ byPositionMode
    Specifies the get profile position specification method.
    Typedef enum {
    LJX8IF_PROFILE_POSITION_CURRENT = 0x00, // From current
    LJX8IF_PROFILE_POSITION_OLDEST = 0x01, // From oldest
    LJX8IF_PROFILE_POSITION_SPEC = 0x02, // Specify position
    } LJX8IF_PROFILE_POSITION;"""
    LJX8IF_PROFILE_POSITION_CURRENT = 0x0
    LJX8IF_PROFILE_POSITION_OLDEST = 0x1
    LJX8IF_PROFILE_POSITION_SPEC = 0x02

    def __init__(self,value):
        self._as_parameters = int(value)

class LJX8IF_GET_PROFILE_RESPONSE(ct.Structure):
    """pRsp(out)
    Indicates the position, etc., of the profiles that were actually acquired.
    Typedef struct {
    DWORD dwCurrentProfileNo; // The latest profile number at the current point in
    time.
    DWORD dwOldestProfileNo;
    DWORD dwGetTopProfileNo;
    BYTE byGetProfileCount;
    BYTE reserve[3];
    } LJX8IF_GET_PROFILE_RESPONSE;
    """
    _fields_ = [('dwCurrentProfileNo', ct.c_uint32),
                ('dwOldestProfileNo', ct.c_uint32),
                ('dwGetTopProfileNo', ct.c_uint32),
                ('byGetProfileCount', ct.c_uint8),
                ('reserve', B_ARRAY_3),
                ]

class LJX8IF_PROFILE_INFO(ct.Structure):
    """
    pProfileInfo(out)
    The profile information for the acquired profiles.
    Typedef struct {
    BYTE byProfileCount; // Fixed to 1.
    BYTE reserve1;
    BYTE byLuminanceOutput; // Brightness
    (1: brightness present, 0: brightness not
    present).
    BYTE reserve2;
    WORD wProfileDataCount; // Profile data count.
    (3200 for the LJ-X and 800 for the LJ-V in the
    initial settings.)
    BYTE reserve3[2];
    LONG lXStart; // X coordinate of point 1.
    LONG lXPitch; // Profile data X direction interval.
    } LJX8IF_PROFILE_INFO;
    """
    _fields_ = [('byProfileCount', ct.c_uint8),
                ('reserve1', ct.c_uint8),
                ('byLuminanceOutput', ct.c_uint8),
                ('reserve2', ct.c_uint8),
                ('wProfileDataCount', ct.c_uint16),
                ('reserve3', B_ARRAY_2),
                ('lXStart', ct.c_int32),
                ('lXPitch', ct.c_int32),
                ]

class LJX8IF_GET_PROFILE_REQUEST(ct.Structure):
    """pReq(in)
    Specifies the position, etc. of the profiles to get.
    Typedef struct {
    BYTE byTargetBank;
    BYTE byPositionMode;
    BYTE reserve[2];
    DWORD dwGetProfileNo;
    BYTE byGetProfileCount;
    BYTE byErase;
    BYTE reserve2[2];
    } LJX8IF_GET_PROFILE_REQUEST;"""
    _fields_ = [('byTargetBank', ct.c_uint8),
                ('byPositionMode', ct.c_uint8),
                ('reserve', B_ARRAY_2),
                ('dwGetProfileNo', ct.c_uint32),
                ('byGetProfileCount', ct.c_uint8),
                ('byErase', ct.c_uint8),
                ('reserve2', B_ARRAY_2),
                ]

class LJX8IF_HIGH_SPEED_PRE_START_REQ(ct.Structure):
    '''
    Specifies which data to start sending in high-speed communication.
    Typedef struct {
    BYTE bySendPosition;
    BYTE reserve[3];
    } LJX8IF_HIGH_SPEED_PRE_START_REQ;
    '''
    _fields_ = [('bySendPosition', ct.c_uint8),
                ('reserve', B_ARRAY_3),
                ]


############ API #############################

class Keyence():

    ''' Keyence profilometer API
    arg: - ip address ex.: '192.168.0.1'
    '''

    def __init__(self, ip_address='192.168.0.2', port=24691, high_speed_port=24692):
        
        self.ip = ip_address
        self.port = port
        self.hs_port = high_speed_port
        self.B_ARRAY_2 = ct.c_uint8 * 2
        self.B_ARRAY_3 = ct.c_uint8 * 3
        self.B_ARRAY_4 = ct.c_uint8 * 4
        self.is_error = False
        self.is_connected = False
        self.HS_acquiring = False

        self.HS_buffer_profiles_Z = []
        self.HS_buffer_profiles_Gray = []

        # Creating callback fct        
        #def callback_fct(pBuffer, dwSize, dwCount, dwNotify, dwUser):
        #    self.py_cmp_func(pBuffer, dwSize, dwCount, dwNotify, dwUser)
        
        #self.callback_function = callback_fct
        # Loading DLL
        #self.kDll = ct.cdll.LoadLibrary('.\KeyenceProfilo\LJX8_IF.dll')
        if ('KeyenceProfilo' in os.getcwd()):
            self.kDll = ct.cdll.LoadLibrary(r'.\LJX8_IF.dll')
        else:
            self.kDll = ct.cdll.LoadLibrary(r'.\lib\KeyenceProfilo\LJX8_IF.dll')
        # Initialization of the profilometer
        self.kDll.LJX8IF_Initialize()

        # Get DLL version
        self.kDll.LJX8IF_GetVersion.restype = LJX8IF_VERSION_INFO
        get_getVersion = self.kDll.LJX8IF_GetVersion()
        print('Keyence DLL version: ',
            get_getVersion.nMajorNumber,
            get_getVersion.nMinorNumber,
            get_getVersion.nRevisionNumber,
            get_getVersion.nBuildNumber)

        # Open ethernet
        ip_list = re.split(r'\W+', self.ip)
        ip = self.B_ARRAY_4(int(ip_list[0]), int(ip_list[1]), int(ip_list[2]), int(ip_list[3]))
        reserve = self.B_ARRAY_2(0,0)
        self.set_IP = LJX8IF_ETHERNET_CONFIG(ip, ct.c_uint16(self.port), reserve)

        self.kDll.LJX8IF_EthernetOpen.argtypes = [ct.c_uint16, ct.POINTER(LJX8IF_ETHERNET_CONFIG)]
        connection_error = self.kDll.LJX8IF_EthernetOpen(ct.c_uint16(0), ct.byref(self.set_IP))
        if connection_error != 0:
            print(f'Connection error')
            self.is_error = True
        else:
            print('Keyence connected')
            self.is_connected = True

    def get_temperatures(self):
        pnTemperatures = [ct.c_short(), ct.c_short(), ct.c_short()]

        self.kDll.LJX8IF_GetHeadTemperature.argtypes = [ct.c_uint16,
            ct.POINTER(ct.c_short),ct.POINTER(ct.c_short),
            ct.POINTER(ct.c_short)]
        self.kDll.LJX8IF_GetHeadTemperature(ct.c_uint16(0), ct.byref(pnTemperatures[0]),ct.byref(pnTemperatures[1]),ct.byref(pnTemperatures[2]))
        return (pnTemperatures[0].value/100, pnTemperatures[1].value/100, pnTemperatures[2].value/100)
    
    def get_profile(self, filename=None):
        bufferSize = ((6+3200*2+1))
        tab_ = [1]*(bufferSize)
        pdwProfileData = (ct.c_int32 * len(tab_))(*tab_)
        dwDataSize = ct.c_uint32(bufferSize*4)

        # LJX8IF_GET_PROFILE_REQUEST
        
        set_LJX8IF_GET_PROFILE_REQUEST = LJX8IF_GET_PROFILE_REQUEST(ct.c_uint8(0), ct.c_uint8(0), 
        B_ARRAY_2(0,0),ct.c_uint32(0), ct.c_uint8(1), ct.c_uint8(0), B_ARRAY_2(0,0))

        get_response = LJX8IF_GET_PROFILE_RESPONSE()
        get_info = LJX8IF_PROFILE_INFO()

        self.kDll.LJX8IF_GetProfile.argtypes = [ct.c_uint16, ct.POINTER(LJX8IF_GET_PROFILE_REQUEST), 
        ct.POINTER(LJX8IF_GET_PROFILE_RESPONSE), ct.POINTER(LJX8IF_PROFILE_INFO),  
        ct.POINTER(ct.c_int32 * len(tab_)), ct.c_uint32]

        e = self.kDll.LJX8IF_GetProfile(ct.c_uint16(0),set_LJX8IF_GET_PROFILE_REQUEST,ct.byref(get_response),
        ct.byref(get_info), ct.byref(pdwProfileData),dwDataSize)
        
        self.f_profile_z = None
        self.f_profile_gray = None
        if (filename):
            
            self.f_profile_z = open(filename[0], 'w')
            self.f_profile_gray = open(filename[1], 'w')
        if (e == 0):
            spectre1 = pdwProfileData[6:3206]
            spectre2 = pdwProfileData[3206:3206+3200]
      
            if (filename):
                np.savetxt(self.f_profile_z, np.expand_dims(spectre1,axis=0), delimiter=';')
                np.savetxt(self.f_profile_gray, np.expand_dims(spectre2,axis=0), delimiter=';')
                
                #f_profile_z.write(str(spectre1).strip('[]')+'\n')
                #f_profile_gray.write(str(spectre2).strip('[]')+'\n')
                self.f_profile_z.close()
                self.f_profile_gray.close()
            return spectre1,spectre2
        else:
            return e

    def HS_start_acquisition(self, filename=None, callback_freq=30):
        """
        filename : tuple of filename (filename_z_profile, filename_gray_profile)
        spectra_freq : int (callback fct called every spectra_freq spectra)
        """
        # Restart
        self.HS_buffer_profiles_Z = []
        self.HS_buffer_profiles_Gray = []

        # Connection configuration
        wHighSpeedPortNo = ct.c_uint16(self.hs_port)

        # Buffer declaration
        bufferSize = ((6+3200*2+1)) * callback_freq
        #pdwProfileData = (ct.c_int32 * bufferSize)()
        
        # If already in acquisition then kill old one
        if (self.HS_acquiring):
            self.HS_stop_acquisition()

        def callback_fct(pBuffer, dwSize, dwCount, dwNotify, dwUser):
            self.HS_acquiring = True
            spectre_buffer = pBuffer[0][:]
            spectre_list = np.fromiter(spectre_buffer, dtype=np.float, count=int(dwCount*dwSize/4))
            spectre_list = spectre_list.reshape((dwCount, int(dwSize/4)))
            self.HS_buffer_profiles_Z.append(spectre_list[:,6:3206])
            self.HS_buffer_profiles_Gray.append(spectre_list[:,3206:3206+3200])
                            
        self.py_cmp_func = ct.CFUNCTYPE(ct.c_void_p, ct.POINTER(ct.c_int32 * bufferSize), ct.c_uint16, ct.c_uint16, ct.c_uint16, ct.c_uint16)(callback_fct)

        # 1 - Init high speed data communication
        self.kDll.LJX8IF_InitializeHighSpeedDataCommunication.argtypes = [ct.c_int32, ct.POINTER(LJX8IF_ETHERNET_CONFIG), ct.c_uint16, ct.c_void_p, ct.c_uint32, ct.c_uint32]
        e = self.kDll.LJX8IF_InitializeHighSpeedDataCommunication(ct.c_int32(0), self.set_IP, wHighSpeedPortNo, self.py_cmp_func, ct.c_uint32(callback_freq), ct.c_uint32(0))

        # 2 - Prestart 
        bySendPosition = ct.c_uint8(2)
        set_LJX8IF_HIGH_SPEED_PRE_START_REQ = LJX8IF_HIGH_SPEED_PRE_START_REQ(bySendPosition, B_ARRAY_3(0,0,0))
        get_info = LJX8IF_PROFILE_INFO()
        self.kDll.LJX8IF_PreStartHighSpeedDataCommunication.argtypes = [ct.c_int32, ct.POINTER(LJX8IF_HIGH_SPEED_PRE_START_REQ), ct.POINTER(LJX8IF_PROFILE_INFO)]
        self.kDll.LJX8IF_PreStartHighSpeedDataCommunication(ct.c_int32(0), set_LJX8IF_HIGH_SPEED_PRE_START_REQ, get_info)
        
        # 3 - Real Start
        self.kDll.LJX8IF_StartHighSpeedDataCommunication.argtypes = [ct.c_int32]

        self.kDll.LJX8IF_StartHighSpeedDataCommunication(ct.c_int32(0))
        
        return e

    def get_buffer_profiles(self, filename=None):
        try:
            HS_array_profiles_Z = np.concatenate(self.HS_buffer_profiles_Z, axis=0)
            HS_array_profiles_Gray = np.concatenate(self.HS_buffer_profiles_Gray, axis=0)
            
            if (filename):
                f_profile_z = open(filename[0], 'w')
                f_profile_gray = open(filename[1], 'w')
                np.savetxt(f_profile_z, HS_array_profiles_Z, delimiter=';')
                np.savetxt(f_profile_gray, HS_array_profiles_Gray, delimiter=';')
                f_profile_z.close()
                f_profile_gray.close()
            
            return HS_array_profiles_Z, HS_array_profiles_Gray
        
        except:
            return 1, 1

    def HS_stop_acquisition(self):
        # 4 - Stop
        self.kDll.LJX8IF_StopHighSpeedDataCommunication.argtypes = [ct.c_int32]
        self.kDll.LJX8IF_StopHighSpeedDataCommunication(ct.c_int32(0))
        
        # 5 - Finalize
        self.kDll.LJX8IF_FinalizeHighSpeedDataCommunication.argtypes = [ct.c_int32]
        self.kDll.LJX8IF_FinalizeHighSpeedDataCommunication(ct.c_int32(0))
        self.HS_acquiring = False
                
        return 0


if __name__ == "__main__":
    K = Keyence(ip_address='192.168.0.1')
    t1, t2, t3 = K.get_temperatures()
    print(t1,t2,t3)
    import time
    time.sleep(1)
    K.HS_start_acquisition()
    time.sleep(5)
    K.HS_stop_acquisition()
    print('ok')
