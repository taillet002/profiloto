#!/usr/bin/env python3
# Software License Agreement (BSD License)
#
# Copyright (c) 2019, UFACTORY, Inc.
# All rights reserved.
#

import os
import sys
import time
sys.path.append(os.path.join(os.path.dirname(__file__), '../../..'))

from xarm.wrapper import XArmAPI

arm = XArmAPI('192.168.1.197')
time.sleep(0.5)
if arm.warn_code != 0:
    arm.clean_warn()
if arm.error_code != 0:
    arm.clean_error()

code = arm.set_gripper_mode(0)
print('set gripper mode: location mode, code={}'.format(code))

code = arm.set_gripper_enable(True)
print('set gripper enable, code={}'.format(code))

code = arm.set_gripper_speed(3000)
print('set gripper speed, code={}'.format(code))

code = arm.set_gripper_position(0, wait=True)
print('[wait]set gripper pos, code={}'.format(code))

def bytes_to_u16(data):
    """大端字节序"""
    data_u16 = data[0] << 8 | data[1]
    return data_u16
def u16_to_bytes(data):
    """大端字节序"""
    bts = bytes([data // 256 % 256])
    bts += bytes([data % 256])
    return bts


ret = arm.core.gripper_modbus_r16s(0x0102, 1)
# print(ret)
print("gripper dir mode:%d"%bytes_to_u16(ret[5:7]))

ret = arm.core.gripper_modbus_r16s(0x0819, 1)
print(ret)
print("motor record joint zero:%d"%(bytes_to_u16(ret[5:7])))

ret = arm.core.gripper_modbus_r16s(0x001F, 1)
print(ret)
print("motor current position:%d"%(bytes_to_u16(ret[5:7])))