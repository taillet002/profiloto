"""
Bunch of useful functions for robotic plateform projects
Solvay - LoF
"""

# to do : delay shift x in core_pos

import ipywidgets as widgets
import numpy as np
import math

def xArmUI(arm):
    
    # We must set the position here to save it in the robot !!!!!
    new_pos = arm.get_servo_angle()
    arm.set_servo_angle(servo_id=None, angle=new_pos[1], relative=False, wait=True)
    
    # 1 - Widgets
    style = {'description_width': 'initial'}
    # 1.a cartesian block
    dist_wid = widgets.FloatText(value=5.0, description='dist (mm)',  layout=widgets.Layout(width='180px'), style=style)
    spd_wid = widgets.FloatText(value=60.0, description='speed (mm/s)',  layout=widgets.Layout(width='180px'), style=style)
    chk_wid = widgets.Checkbox(value=False, description='mode TCP', tooltip='Activate (or not) TCP relative movements', disabled=False, indent=False)
    up_wid = widgets.Button(description='upward +z', disabled=False, button_style='', tooltip='go upward for distance value', icon='', style=widgets.ButtonStyle(button_color='#4e8ac7'))
    forward_wid = widgets.Button(description='forward +x', disabled=False, button_style='', tooltip='go forward for distance value', icon='',style=widgets.ButtonStyle(button_color='lightblue'))
    cleanerr_wid = widgets.Button(description='clean errors', disabled=False, button_style='', tooltip='clean errors', icon='',style=widgets.ButtonStyle(button_color='#784441'))
    left_wid = widgets.Button(description='left -y', disabled=False, button_style='', tooltip='go left for distance value', icon='', style=widgets.ButtonStyle(button_color='lightblue'))
    reset_wid = widgets.Button(description='RESET', disabled=False, button_style='', tooltip='Clear error and go home action', icon='', style=widgets.ButtonStyle(button_color='#bd8f8f'))
    right_wid = widgets.Button(description='right +y', disabled=False, button_style='', tooltip='go right for distance value', icon='', style=widgets.ButtonStyle(button_color='lightblue'))
    down_wid = widgets.Button(description='downward -z', disabled=False, button_style='', tooltip='go downward for distance value', icon='', style=widgets.ButtonStyle(button_color='#4e8ac7'))
    backyard_wid = widgets.Button(description='backward -x', disabled=False, button_style='', tooltip='go backward for distance value', icon='', style=widgets.ButtonStyle(button_color='lightblue'))
    roll_wid = widgets.Button(description='roll x', disabled=False, button_style='', tooltip='rotation on roll axis', icon='', style=widgets.ButtonStyle(button_color='#c9d9ff'))
    pitch_wid = widgets.Button(description='pitch y', disabled=False, button_style='', tooltip='rotation on pitch axis', icon='', style=widgets.ButtonStyle(button_color='#c9d9ff'))
    yaw_wid = widgets.Button(description='yaw z', disabled=False, button_style='', tooltip='rotation on yaw axis', icon='', style=widgets.ButtonStyle(button_color='#c9d9ff'))
    angle_wid = widgets.FloatText(value=5.0, description='angle (°)',  layout=widgets.Layout(width='180px'), style=style)
    # 1.b servo angles block
    servo_a_wid = widgets.FloatText(value=5.0, description='servo angle (°)',  layout=widgets.Layout(width='180px'), style=style)
    spd_servo_wid = widgets.FloatText(value=20.0, description='servo speed (°/s)',  layout=widgets.Layout(width='180px'), style=style)
    serv01_wid = widgets.Button(description='servo 1', disabled=False, button_style='', tooltip='servo ID = 1', icon='', style=widgets.ButtonStyle(button_color='lightgreen'))
    serv02_wid = widgets.Button(description='servo 2', disabled=False, button_style='', tooltip='servo ID = 2', icon='', style=widgets.ButtonStyle(button_color='lightgreen'))
    serv03_wid = widgets.Button(description='servo 3', disabled=False, button_style='', tooltip='servo ID = 3', icon='', style=widgets.ButtonStyle(button_color='lightgreen'))
    serv04_wid = widgets.Button(description='servo 4', disabled=False, button_style='', tooltip='servo ID = 4', icon='', style=widgets.ButtonStyle(button_color='lightgreen'))
    serv05_wid = widgets.Button(description='servo 5', disabled=False, button_style='', tooltip='servo ID = 5', icon='', style=widgets.ButtonStyle(button_color='lightgreen'))
    serv06_wid = widgets.Button(description='servo 6', disabled=False, button_style='', tooltip='servo ID = 6', icon='', style=widgets.ButtonStyle(button_color='lightgreen'))
    # 1.c gripper block
    grip_dist = widgets.FloatText(value=5.0, description='gripper dist (mm)',  layout=widgets.Layout(width='200px'))
    open_wid = widgets.Button(description='open', disabled=False, button_style='', tooltip='open gripper', icon='', style=widgets.ButtonStyle(button_color='#e4aaf0'))
    close_wid = widgets.Button(description='close', disabled=False, button_style='', tooltip='close gripper', icon='', style=widgets.ButtonStyle(button_color='#e4aaf0'))
    # 1.d manual block
    manON_wid = widgets.Button(description='ON', disabled=False, button_style='', tooltip='Manual mode ON', icon='', style=widgets.ButtonStyle(button_color='#fcebae'))
    manOFF_wid = widgets.Button(description='OFF', disabled=False, button_style='', tooltip='Manual mode OFF', icon='', style=widgets.ButtonStyle(button_color='#fcebae'))
    # 1.e go foward (backyard) block
    distgoforw_wid = widgets.FloatText(value=10, description='dist (mm)',  layout=widgets.Layout(width='180px'), style=style)
    goforw_wid = widgets.Button(description='Go', disabled=False, button_style='', tooltip='Go forward', icon='', style=widgets.ButtonStyle(button_color='#30732a'))
    # 1.e printer block
    printpos_wid = widgets.Button(description='printpos', disabled=False, button_style='', tooltip='print positions', icon='', style=widgets.ButtonStyle(button_color='yellow'))

    output = widgets.Output()

    global dist;                dist = 5
    global spd;                 spd = 60
    global angle;               angle = 5
    global spd_servo;           spd_servo = 20
    global servo_angle;         servo_angle = 5
    global gripper_dist;        gripper_dist = 5
    global goforw_dist;         goforw_dist = 10
    global modeTCP;             modeTCP = False

    # 2 - Function when modification detected
    def move(change):
        global dist, spd, modeTCP
        if type(change) is not str:
            xdist=0; ydist=0; zdist=0; rolldeg=0; pitchdeg=0; yawdeg=0
            if change.get_state()['description'] == 'forward +x':
                xdist = dist
            elif change.get_state()['description'] == 'backward -x':
                xdist = -dist
            elif change.get_state()['description'] == 'left -y':
                ydist = -dist
            elif change.get_state()['description'] == 'right +y':
                ydist = dist
            elif change.get_state()['description'] == 'upward +z':
                zdist = dist
            elif change.get_state()['description'] == 'downward -z':
                zdist = -dist
            elif change.get_state()['description'] == 'roll x':
                rolldeg = angle
            elif change.get_state()['description'] == 'pitch y':
                pitchdeg = angle
            elif change.get_state()['description'] == 'yaw z':
                yawdeg = angle

            with output:
                if modeTCP:
                    arm.set_tool_position(x=xdist, y=ydist, z=zdist, roll=rolldeg, pitch=pitchdeg, yaw=yawdeg, speed=spd, is_radian=False, wait=True)
                else:
                    arm.set_position(x=xdist, y=ydist, z=zdist, roll=rolldeg, pitch=pitchdeg, yaw=yawdeg, speed=spd, is_radian=False, wait=True, relative=True)         

    def change_dist(change):
        global dist
        dist = dist_wid.value

    def change_spd(change):
        global spd
        spd = spd_wid.value

    def change_modeTCP(change):
        global modeTCP
        modeTCP = chk_wid.value

    def change_angle(change):
        global angle
        angle = angle_wid.value

    def change_reset(change):
        if type(change) is not str:
            with output:
                arm.reset(wait=True)
                # We must set the position here to save it in the robot !!!!!
                new_pos = arm.get_servo_angle()
                arm.set_servo_angle(servo_id=None, angle=new_pos[1], relative=False, wait=True)

    def printpos_fun(change):
        with output:
            output.clear_output()
            print(f'cartesian pos: {arm.get_position()[1]}\nservo pos: {arm.get_servo_angle()[1]}\ngripper pos: {arm.get_gripper_position()[1]}\nTCP offset:{arm.tcp_offset}')

    def change_servo_angle(change):
        global servo_angle
        servo_angle = servo_a_wid.value

    def change_spd_servo(change):
        global spd_servo
        spd_servo = spd_servo_wid.value

    def servo_move(change):
        global spd_servo
        if type(change) is not str:
            angles = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]     # must be float type number
            if change.get_state()['description'] == 'servo 1':
                angles[0] = servo_angle
            elif change.get_state()['description'] == 'servo 2':
                angles[1] = servo_angle
            elif change.get_state()['description'] == 'servo 3':
                angles[2] = servo_angle
            elif change.get_state()['description'] == 'servo 4':
                angles[3] = servo_angle
            elif change.get_state()['description'] == 'servo 5':
                angles[4] = servo_angle
            elif change.get_state()['description'] == 'servo 6':
                angles[5] = servo_angle

            with output:
                arm.set_servo_angle(servo_id=None, angle=angles, speed=spd_servo, relative=True, wait=True)
                
    def change_grip_dist(change):
        global gripper_dist
        gripper_dist = grip_dist.value

    def gripper_move(change):
        if type(change) is not str:
            grip_pos = arm.get_gripper_position()[1]
            if change.get_state()['description'] == 'open':
                new_grip_pos = grip_pos + gripper_dist
            elif change.get_state()['description'] == 'close':
                new_grip_pos = grip_pos - gripper_dist
            
            with output:
                arm.set_gripper_position(new_grip_pos, speed=None, wait=True)     # 0 closed

    def manual_mode_change(change):
        if type(change) is not str:
            if change.get_state()['description'] == 'ON':
                with output:
                    # Turn on manual mode
                    arm.set_mode(2)
                    arm.set_state(0)
                    output.clear_output()
                    print(f'Manual mode ON')
            if change.get_state()['description'] == 'OFF':
                with output:
                    # Turn off manual mode after recording
                    arm.set_mode(0)
                    arm.set_state(0)
                    # Note the new position (cartesian or servo angle)
                    new_pos = arm.get_servo_angle()
                    # We must set the position here to save it in the robot !!!!!
                    arm.set_servo_angle(servo_id=None, angle=new_pos[1], relative=False, wait=True)
                    output.clear_output()
                    print(f'Manual mode OFF')

    def from_RPY_to_uV(roll, pitch, yaw):
        ''' takes roll pitch yaw angles from the TPC and transforms it into unit vector
            http://planning.cs.uiuc.edu/node102.html
            https://www.raspberrypi.org/forums/viewtopic.php?t=109069 '''
        
        alpha = np.radians(yaw)      # yaw
        beta = np.radians(pitch)        # pitch
        gamma = np.radians(roll)        # roll

        # inverse correction
        r1 = [np.cos(alpha)*np.cos(beta),
            np.cos(alpha)*np.sin(beta)*np.sin(gamma) - np.sin(alpha)*np.cos(gamma),
            np.cos(alpha)*np.sin(beta)*np.cos(gamma) + np.sin(alpha)*np.sin(gamma)]
        r2 = [np.sin(alpha)*np.cos(beta),
            np.sin(alpha)*np.sin(beta)*np.sin(gamma) + np.cos(alpha)*np.cos(gamma),
            np.sin(alpha)*np.sin(beta)*np.cos(gamma) - np.cos(alpha)*np.sin(gamma)]
        r3 = [-np.sin(beta), np.cos(beta)*np.sin(gamma),
            np.cos(beta) * np.cos(gamma)]
        
        R = np.vstack([r1, r2, r3])
        
        # by default on the xArm, TCP(roll,pitch,yaw) = (-180,0,0) correspond to (0,0,1) of the main référence coordinates
        u_ref = np.array([0,0,1])
        
        return np.matmul(R, u_ref)

    def get_coord_togoforward(origin=[], dist=10):
        ''' takes origine position [x, y, z, roll, pitch, yaw] and transforms to new strait forward postitions '''
        uV = from_RPY_to_uV(origin[3], origin[4], origin[5])    # unit vector
        print(uV)
        x = origin[0] + uV[0] * dist
        y = origin[1] + uV[1] * dist
        z = origin[2] + uV[2] * dist

        return [x, y, z, origin[3], origin[4], origin[5]]

    def change_goforw_dist(change):
        global goforw_dist
        goforw_dist = distgoforw_wid.value

    def go_forward_move(change):
        if type(change) is not str:
            pos = arm.get_position()[1]
            
            new_coord = get_coord_togoforward(pos, dist=goforw_dist)

            with output:
                arm.set_position(*new_coord, speed=spd, relative=False, is_radian=False, wait=True)

    def clean_all_errors(cahgne):
        with output:
            output.clear_output()
            
            if arm.has_error:
                print(f'error was: {arm.error_code}')
            arm.clean_error()
            arm.set_mode(0)
            arm.set_state(state=0)
           
            arm.clean_gripper_error()
            arm.set_gripper_enable(enable=True)
            arm.set_gripper_mode(0)
           
            print(f'all errors cleaned')

    # 3 - Observers
    dist_wid.observe(change_dist, names="value")
    spd_wid.observe(change_spd, names="value")
    chk_wid.observe(change_modeTCP, names="value")
    up_wid.on_click(move)
    forward_wid.on_click(move)
    cleanerr_wid.on_click(clean_all_errors)
    left_wid.on_click(move)
    reset_wid.on_click(change_reset)
    right_wid.on_click(move)
    down_wid.on_click(move)
    backyard_wid.on_click(move)
    angle_wid.observe(change_angle, names="value")
    roll_wid.on_click(move)
    pitch_wid.on_click(move)
    yaw_wid.on_click(move)
    servo_a_wid.observe(change_servo_angle, names="value")
    spd_servo_wid.observe(change_spd_servo, names="value")
    serv01_wid.on_click(servo_move)
    serv02_wid.on_click(servo_move)
    serv03_wid.on_click(servo_move)
    serv04_wid.on_click(servo_move)
    serv05_wid.on_click(servo_move)
    serv06_wid.on_click(servo_move)
    grip_dist.observe(change_grip_dist, names="value")
    open_wid.on_click(gripper_move)
    close_wid.on_click(gripper_move)
    manON_wid.on_click(manual_mode_change)
    manOFF_wid.on_click(manual_mode_change)
    distgoforw_wid.observe(change_goforw_dist, names="value")
    goforw_wid.on_click(go_forward_move)
    printpos_wid.on_click(printpos_fun)

    move("doesn't matter what I send here, just triggering") # MUST be done before widgets.VBox - if response(x) is last, NOTHING is drawn! 

    return widgets.VBox([
        widgets.Label(value="Cartesian block (relative moves) -------------------------------------"),
        widgets.HBox([dist_wid, spd_wid, chk_wid]),
        widgets.HBox([up_wid, forward_wid, cleanerr_wid]),
        widgets.HBox([left_wid, reset_wid, right_wid]),
        widgets.HBox([down_wid, backyard_wid]),
        widgets.HBox([angle_wid, roll_wid, pitch_wid, yaw_wid]),
        widgets.Label(value="Servo angles block (relative moves) ----------------------------------"),
        widgets.HBox([servo_a_wid, spd_servo_wid]),
        widgets.HBox([serv01_wid, serv02_wid, serv03_wid, serv04_wid, serv05_wid, serv06_wid]),
        widgets.Label(value="Gripper block (relative moves) ---------------------------------------"),
        widgets.HBox([grip_dist, open_wid, close_wid]),
        widgets.Label(value="Manual block ---------------------------------------------------------"),
        widgets.HBox([manON_wid, manOFF_wid]),
        widgets.Label(value="Go forward (backward) from actual tilt (relative moves) --------------"),
        widgets.HBox([distgoforw_wid, goforw_wid]),
        widgets.Label(value="Printer button -------------------------------------------------------"),
        printpos_wid,
        output,
    ])

def cor_pos(rackobj, iRow, iCol, angle, delta_z, shift_x=0):
    '''
    from a given position of a rack (rackobj, iRow, iCol) returns a new cartesian position at a given heigh (delta z) and corrected by the angle of the plate
    STEP 1: unit vector along the rack direction is computed from extrems positions 
    STEP 2: the vector is used to decompose the coordinates  
    '''
    # unit vector (alogn the rack)
    u1 = (rackobj['x'][0][0] - rackobj['x'][0][-1])
    u2 = (rackobj['y'][0][0] - rackobj['y'][0][-1])
    u3 = (rackobj['z'][0][0] - rackobj['z'][0][-1])
    norm = np.sqrt(u1**2+u2**2+u3**2)
    u = u1/norm, u2/norm, u3/norm
    #print('x unit vect', u)

    # orthogonal vector in the plane of the rack 
    temp = np.sqrt((u[1]/u[0])**2 + 1)

    v2 = 1/temp
    v1 =  -(u[1]/u[0])*v2
    v3 =  0 # we suppose flat rack

    v = v1, v2, v3
    # print('y unit vect', v)
    
    # correction in x, y, z due to angle
    angle = 90 - angle
    
    angle = np.radians(angle)
    
    dx = np.cos(angle) * delta_z * u[0]
    dy = np.cos(angle) * delta_z * u[1]
    dz = delta_z*np.sin(angle)
    z = rackobj['z'][iRow][iCol] + dz
    x = rackobj['x'][iRow][iCol] + dx
    y = rackobj['y'][iRow][iCol] + dy
    
    # shift the move to the shift_x
    dx = shift_x*v[0]
    dy = shift_x*v[1]
    print(dx,dy)
    x += dx
    y += dy

    new_coord = [x, y, z, rackobj['roll'][iRow][iCol], rackobj['pitch'][iRow][iCol], rackobj['yaw'][iRow][iCol]]

    return new_coord

def get_cart_coord_pos(rack_obj, row_pos, col_pos):
    '''
    return a list of coordinates for 1 position in the rack 
    '''
    return list(rack_obj['x'][row_pos][col_pos], rack_obj['y'][row_pos][col_pos], rack_obj['z'][row_pos][col_pos], rack_obj['roll'][row_pos][col_pos], rack_obj['pitch'][row_pos][col_pos], rack_obj['yaw'][row_pos][col_pos])

def from_RPY_to_uV(roll, pitch, yaw):
    ''' takes roll pitch yaw angles from the TPC and transforms it into unit vector
        http://planning.cs.uiuc.edu/node102.html
        https://www.raspberrypi.org/forums/viewtopic.php?t=109069 '''
    
    alpha = np.radians(yaw)      # yaw
    beta = np.radians(pitch)        # pitch
    gamma = np.radians(roll)        # roll

    # r1 = [np.cos(alpha)*np.cos(beta),
    #     np.sin(alpha)*np.cos(beta),
    #     -np.sin(beta)]
    # r2 = [np.cos(alpha)*np.sin(beta)*np.sin(gamma) - np.sin(alpha)*np.cos(gamma),
    #     np.sin(alpha)*np.sin(beta)*np.sin(gamma) + np.cos(alpha)*np.cos(gamma),
    #     np.cos(beta) * np.sin(gamma)]
    # r3 = [np.cos(alpha)*np.sin(beta)*np.cos(gamma) + np.sin(alpha)*np.sin(gamma),
    #     np.sin(alpha)*np.sin(beta)*np.cos(gamma) - np.cos(alpha)*np.sin(gamma),
    #     np.cos(beta)*np.cos(gamma)]

    # inverse correction
    r1 = [np.cos(alpha)*np.cos(beta),
        np.cos(alpha)*np.sin(beta)*np.sin(gamma) - np.sin(alpha)*np.cos(gamma),
        np.cos(alpha)*np.sin(beta)*np.cos(gamma) + np.sin(alpha)*np.sin(gamma)]
    r2 = [np.sin(alpha)*np.cos(beta),
        np.sin(alpha)*np.sin(beta)*np.sin(gamma) + np.cos(alpha)*np.cos(gamma),
        np.sin(alpha)*np.sin(beta)*np.cos(gamma) - np.cos(alpha)*np.sin(gamma)]
    r3 = [-np.sin(beta), np.cos(beta)*np.sin(gamma),
        np.cos(beta) * np.cos(gamma)]
    
    R = np.vstack([r1, r2, r3])
    
    # by default on the xArm, TCP(roll,pitch,yaw) = (-180,0,0) correspond to (0,0,1) of the main référence coordinates
    u_ref = np.array([0,0,1])
    
    return np.matmul(R, u_ref)

def get_coord_togoforward(origin=[], dist=10):
    ''' takes origine position [x, y, z, roll, pitch, yaw] and transforms to new strait forward postitions '''
    uV = from_RPY_to_uV(origin[3], origin[4], origin[5])    # unit vector
    print(uV)
    x = origin[0] + uV[0] * dist
    y = origin[1] + uV[1] * dist
    z = origin[2] + uV[2] * dist

    return [x, y, z, origin[3], origin[4], origin[5]]

def set_local_orientation(local_theta, first_servo_angle, is_radian = None):
    # Global parameters for frame trnsfer
    theta_base = [0.0, -90.0, 0.0] 

    # Usefull Functions
    # Calculates Rotation Matrix given euler angles.
    def eulerAnglesToRotationMatrix(theta) :
        
        R_x = np.array([[1,         0,                  0                   ],
                        [0,         math.cos(theta[0]), -math.sin(theta[0]) ],
                        [0,         math.sin(theta[0]), math.cos(theta[0])  ]
                        ])
                        
        R_y = np.array([[math.cos(theta[1]),    0,      math.sin(theta[1])  ],
                        [0,                     1,      0                   ],
                        [-math.sin(theta[1]),   0,      math.cos(theta[1])  ]
                        ])
                    
        R_z = np.array([[math.cos(theta[2]),    -math.sin(theta[2]),    0],
                        [math.sin(theta[2]),    math.cos(theta[2]),     0],
                        [0,                     0,                      1]
                        ])
                        
        R = np.dot(R_z, np.dot( R_y, R_x ))

        return R

    # Checks if a matrix is a valid rotation matrix.
    def isRotationMatrix(R) :
        Rt = np.transpose(R)
        shouldBeIdentity = np.dot(Rt, R)
        I = np.identity(3, dtype = R.dtype)
        n = np.linalg.norm(I - shouldBeIdentity)
        return n < 1e-6

    # Calculates rotation matrix to euler angles
    # The result is the same as MATLAB except the order
    # of the euler angles ( x and z are swapped ).
    def rotationMatrixToEulerAngles(R) :

        assert(isRotationMatrix(R))
        
        sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
        
        singular = sy < 1e-6

        if  not singular :
            x = math.atan2(R[2,1] , R[2,2])
            y = math.atan2(-R[2,0], sy)
            z = math.atan2(R[1,0], R[0,0])
        else :
            x = math.atan2(-R[1,2], R[1,1])
            y = math.atan2(-R[2,0], sy)
            z = 0

        return np.array([x, y, z])

    def radian_to_degree(theta_radian):
        return [i*180/math.pi for i in theta_radian]

    def degree_to_radian(theta_degree):
        return [i*math.pi/180 for i in theta_degree]

    # Determine the rotation matrix with new convention
    def rotation_matrix_new_system(theta):
        R_x = np.array([[1,         0,                  0                   ],
                        [0,         math.cos(theta[0]), -math.sin(theta[0]) ],
                        [0,         math.sin(theta[0]), math.cos(theta[0])  ]
                        ])
                        
        R_y = np.array([[math.cos(theta[1]),    0,      math.sin(theta[1])  ],
                        [0,                     1,      0                   ],
                        [-math.sin(theta[1]),   0,      math.cos(theta[1])  ]
                        ])
                    
        R_z = np.array([[math.cos(theta[2]),    -math.sin(theta[2]),    0],
                        [math.sin(theta[2]),    math.cos(theta[2]),     0],
                        [0,                     0,                      1]
                        ])

        R = np.dot(R_x, np.dot( R_y, R_z ))

        return R

    # Main function
    def local_frame_to_global_frame(loc_theta, base_theta, servo_angle_0, radius=None):
        # Check if the list given are of the appropriate size
        if((len(base_theta)!=3) or (len(loc_theta)!=3)):
            return False, np.array([0, 0, 0])

        # Extract right angles from system
        theta = [loc_theta[i]+base_theta[i] for i in range(min(len(loc_theta),len(base_theta)))]
        theta[2] += servo_angle_0

        # Check if working with radius or degrees
        if(radius != True):
            theta = degree_to_radian(theta)
        
        # Compute rotation matrix in new system
        R = rotation_matrix_new_system(theta)

        # Compute back the Euler corresponding angles
        euler_corresponding_angles = rotationMatrixToEulerAngles(R)
        if(radius):
            return True, euler_corresponding_angles     # this is a np.array([..., ..., ...])
        else:
            return True, radian_to_degree(euler_corresponding_angles)
    
    # Output of the function (Test, np.array([..., ..., ...]))
    # If Test == False, the result is not reliable
    return local_frame_to_global_frame(local_theta, theta_base, first_servo_angle, radius = is_radian)


if __name__ == "__main__":
    print('ok')
    import json
    with open(r'.\rack1.json', 'r') as fp:
        r1 = json.load(fp)
    cor_pos(r1,0,0,20,50,50)
