""" TO DO : 
today roll pitch yaw is overwrite by the first one ==> to be improve
add 'allcoords' 
"""

import numpy as np
import json

class Rack():

    def __init__(self, nRows, nCols, name='defaultName', sim_On=False):
        ''' Class Rack allows the user to automaticaly build racks coordinates
        by giving 3 informations:
            - nRows: the number of row of the rack
            - nCols: the number of columns of the rack
            - refPoints: 4 corners references points of the rack
        Please see set_calibration help
        positions: [x, y, z, roll, pitch, yaw] '''
        self.name = name                # name of the rack
        self.nRows = nRows              # nRows: the number of row of the rack
        self.nCols = nCols              # nCols: the number of columns of the rack   
        self.general_safe_z = 0         # a general safe Z position at the center of the rack
        self.refPoints = []             # refPoints: 4 corners references points of the rack

        # individual postitions calculated with set_calibrattion
        self.individual_pos = np.zeros((nRows, nCols, 6))
        self.individual_pos_safe_z = np.zeros((nRows, nCols, 6))
        # individual postitions calculated when set_calibrattion + z safe heigh
        self.individual_safe_z = 0
       
        if sim_On:
            self.refPoints = [
                [10, 5, 0.1, 0, 0, 0],
                [10, -15, 0.5, 0, 0, 0],
                [60, 5, 0, -0.3, 0, 0],
                [60, -15, -0.2, 0, 0, 0],
            ]
        
    def set_calibration(self, indiv_safe_z=0):
        ''' After having set refPoints (4 corners references points of the rack) :
            refpoints: list of list of rack corners positions
                [   [x, y, z, pitch, roll, yaw] # top left corner
                    [x, y, z, pitch, roll, yaw] # bottom left corner
                    [x, y, z, pitch, roll, yaw] # top right corner
                    [x, y, z, pitch, roll, yaw] # bottom right corner
                ]
            (You can give a list of 2 ref points only for one row racks)
            Use set_calibration to build a numpy matrice of the rack shape (nRow, nCols) with the coordinates

            output :
                individual_pos --> numpy matrice of the rack shape (nRow, nCols) with the coordinates
                individual_safe_z --> equal "individual_pos" but with a Z offset defined by indiv_safe_z
         '''
        
        if not self.refPoints:
            print('please use set_refPoints() before')
            return 1

        # calculate individual positions
        self.individual_pos = np.zeros((self.nRows, self.nCols, 6))

        # if only 2 points reference duplicate to get 4
        if len(self.refPoints) == 2:
            self.refPoints.insert(1, self.refPoints[0])
            self.refPoints.insert(-1, self.refPoints[-1])

        # BUILD a numpy matrice of the rack shape (nRow, nCols) with the coordinates
        
        # 1. 1st column linespace (fcl) between point 1 and point 2
        fcl_x = np.linspace(self.refPoints[0][0], self.refPoints[1][0], self.nRows)
        fcl_y = np.linspace(self.refPoints[0][1], self.refPoints[1][1], self.nRows)
        fcl_z = np.linspace(self.refPoints[0][2], self.refPoints[1][2], self.nRows)
        self.individual_pos[:, 0, 0] = fcl_x
        self.individual_pos[:, 0, 1] = fcl_y
        self.individual_pos[:, 0, 2] = fcl_z
        
        # 2. last column linespace (lcl) between point 3 and point 4
        lcl_x = np.linspace(self.refPoints[2][0], self.refPoints[3][0], self.nRows)
        lcl_y = np.linspace(self.refPoints[2][1], self.refPoints[3][1], self.nRows)
        lcl_z = np.linspace(self.refPoints[2][2], self.refPoints[3][2], self.nRows)
        self.individual_pos[:, -1, 0] = lcl_x
        self.individual_pos[:, -1, 1] = lcl_y
        self.individual_pos[:, -1, 2] = lcl_z
        
        # 3. loop on all rows applying linspace regarding nCols
        for row in range(self.nRows):
            self.individual_pos[row, :, 0] = np.linspace(self.individual_pos[row,0,0], self.individual_pos[row,-1,0], self.nCols)  # x
            self.individual_pos[row, :, 1] = np.linspace(self.individual_pos[row,0,1], self.individual_pos[row,-1,1], self.nCols)  # y
            self.individual_pos[row, :, 2] = np.linspace(self.individual_pos[row,0,2], self.individual_pos[row,-1,2], self.nCols)  # z

        # 4. take (roll, pitch, yaw) of 1st refPoints and apply it to all positions
        self.individual_pos[:, :, 3] = self.refPoints[0][3]
        self.individual_pos[:, :, 4] = self.refPoints[0][4]
        self.individual_pos[:, :, 5] = self.refPoints[0][5]

        # Create a individual_safe_z only Z safe pos
        self.individual_safe_z = self.individual_pos[:, :, 2].copy()
        self.individual_safe_z = self.individual_safe_z + indiv_safe_z
        
        # Create a individual_pos_safe_z rack positions equivalent to individual_pos but with a different z
        self.individual_pos_safe_z = self.individual_pos.copy()
        self.individual_pos_safe_z[:, :, 2] = self.individual_safe_z

        return self.individual_pos, self.individual_safe_z

    def set_general_safe_z(self, general_safe_z=0):
        ''' take the center of the rack + a safe Z '''
        if not self.refPoints:
            print('please add a refPoints variable')
            return 1
        
        np_ref = np.array(self.refPoints)
        
        # [x, y, z, roll, pitch, yaw]
        self.general_safe_z = [
            np.sum(np_ref[:,0])/np_ref.shape[0],                    # x
            np.sum(np_ref[:,1])/np_ref.shape[0],                    # y
            np.sum(np_ref[:,2])/np_ref.shape[0] + general_safe_z,   # z
            0, 0, 0]                                                # roll, pitch, yaw
        
        return self.general_safe_z

    def save_config(self, filename):
        """ This function aims at saving coordinates in json file """
        savedDict = {
            'name': self.name,
            'nRows': self.nRows,
            'nCols': self.nCols,
            'x': self.individual_pos[:, :, 0].tolist(),
            'y': self.individual_pos[:, :, 1].tolist(),
            'z': self.individual_pos[:, :, 2].tolist(),
            'z_safe': self.individual_safe_z.tolist(),
            'roll': self.individual_pos[:, :, 3].tolist(),
            'pitch': self.individual_pos[:, :, 4].tolist(),
            'yaw': self.individual_pos[:, :, 5].tolist(),
            'allcoords': self.individual_pos.tolist(),
            'allcoords_z_safe': self.individual_pos_safe_z.tolist(),
            'general_z_safe': self.general_safe_z,
        }

        with open(filename +'.json', 'w') as fp:
            json.dump(savedDict, fp, indent=4)

    def load_config(self, filename, path=""):
        # not emplemented yep
        return 1


if __name__ == "__main__":

    # Example on how it works
    # example 1:
    # 1 - instanciate a rack by giving the number of rows and columns
    r1 = Rack(3, 15, sim_On=False)
    # r1 rack shape:
    # ooooooooooooooo
    # ooooooooooooooo
    # ooooooooooooooo
    # 2 - define references points
    r1.refPoints = [[10, 50, 0.1, 0, 0, 0],
                    [30, 52, 0.1, 0, 0, 0],
                    [10, 55, 0.3, 0, 0, 0],
                    [30, 56, 0.2, 0, 0, 0]]
    # 3 - set_calibration: a numpy matrice of the rack shape (nRow, nCols) with the coordinates
    pos, pos_safe_z = r1.set_calibration(indiv_safe_z=50)
    # 4 - get a general safe Z of the rack
    gsz_pos = r1.set_general_safe_z(150)
    # 5 - save a json config of the rack
    r1.save_config('plate_pos')

    # example 2, a one row rack:
    # 1 - instanciate a rack by giving the number of rows and columns
    r2 = Rack(1, 17, sim_On=False)
    # r2 rack shape:
    # ooooooooooooooooo
    # 2 - define references points
    r2.refPoints = [[10, 50, 0.1, 0, 0, 0], [60, 55, 0.3, 0, 0, 0]]
    # 3 - set_calibration: a numpy matrice of the rack shape (nRow, nCols) with the coordinates
    pos, pos_safe_z = r2.set_calibration(indiv_safe_z=50)
    # 4 - get a general safe Z of the rack
    gsz_pos = r1.set_general_safe_z(150)
    # 5 - save a json config of the rack
    r2.save_config('plate_pos')